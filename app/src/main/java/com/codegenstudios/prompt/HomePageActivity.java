package com.codegenstudios.prompt;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class HomePageActivity extends AppCompatActivity {
    TextView txt_AddLRBook,txt_srnDetails,txt_bookingList,historyandsrn;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_newmain);
        txt_AddLRBook=(TextView)findViewById(R.id.txt_AddLRBook);
        txt_srnDetails=(TextView)findViewById(R.id.txt_srnDetails);
        txt_bookingList=(TextView)findViewById(R.id.txt_bookingList);
        historyandsrn=(TextView)findViewById(R.id.historyandsrn);
        txt_AddLRBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(HomePageActivity.this,LRBookDetailsActivity.class);
                startActivity(i);
            }
        });
        txt_srnDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(HomePageActivity.this,SRNDetailsActivity.class);
                startActivity(i);
            }
        });
        txt_bookingList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(HomePageActivity.this,BookingListActivity.class);
                startActivity(i);
            }
        });
        historyandsrn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(HomePageActivity.this,HistoryAndSRNActivity.class);
                startActivity(i);
            }
        });
    }
}
