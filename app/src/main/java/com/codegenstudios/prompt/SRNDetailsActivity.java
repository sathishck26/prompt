package com.codegenstudios.prompt;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

/**
 * Created by sathish on 11/19/2017.
 */
public class SRNDetailsActivity extends AppCompatActivity {
    Button btn_BillSend;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_srndetails);
        btn_BillSend=(Button)findViewById(R.id.btn_BillSend);
        btn_BillSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(SRNDetailsActivity.this,PaymentActivity.class);
                startActivity(i);
            }
        });
    }
}