package com.codegenstudios.prompt;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

/**
 * Created by sathish on 11/19/2017.
 */
public class TripActivity extends AppCompatActivity {
    Button btn_start,btn_finish,btn_cancel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_trip);
        btn_start=(Button)findViewById(R.id.btn_start);
        btn_finish=(Button)findViewById(R.id.btn_finish);
        btn_cancel=(Button)findViewById(R.id.btn_cancel);
        btn_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(TripActivity.this,TripStartActivity.class);
                startActivity(i);
            }
        });
        btn_finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(TripActivity.this,TripCloseActivity.class);
                startActivity(i);
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(TripActivity.this,TripCancelActivity.class);
                startActivity(i);
            }
        });
    }

}