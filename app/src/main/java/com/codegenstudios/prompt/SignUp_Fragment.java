package com.codegenstudios.prompt;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.codegenstudios.prompt.APIServiceCall.Retrofit.api.ApiService;
import com.codegenstudios.prompt.APIServiceCall.Retrofit.api.RetroClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUp_Fragment extends Fragment implements OnClickListener,AdapterView.OnItemSelectedListener {
	private static View view;
	private static EditText fullName, emailId, mobileNumber,
			password, confirmPassword;
	private static TextView login,txt_backtoview;
	private static Button signUpButton;
	private static CheckBox terms_conditions;
	private static FragmentManager fragmentManager;
	String[] state = { "Andhra Pradesh", "Arunachal Pradesh", "Jammu & Kashmir", "Tamil Nadu", "Telangana ","Rajasthan" };
	String[] distric = { "Ariyalur", "Karur", "Erode", "Perambalur", "Chennai", "Other" };
	String[] region = { "testcase1", "testcase2", "testcase3", "testcase4", "Other",  };
	String[] businesstype = { "Business associate", "Customer", "Sales Person", "Other",  };
	public SignUp_Fragment() {

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.signup_layout, container, false);
		initViews();
		setListeners();

		Spinner spin = (Spinner)view.findViewById(R.id.Select_State);
		Spinner spin1 = (Spinner)view.findViewById(R.id.Select_District);
		Spinner spin2 = (Spinner)view.findViewById(R.id.Select_Region);
		Spinner spin3 = (Spinner)view.findViewById(R.id.Select_BusinessType);
		spin.setOnItemSelectedListener(this);

		//Creating the ArrayAdapter instance having the country list
		ArrayAdapter aa = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item,state);
		aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		ArrayAdapter aa1 = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item,distric);
		aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		ArrayAdapter aa2 = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item,region);
		aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		ArrayAdapter aa3 = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item,businesstype);
		aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		//Setting the ArrayAdapter data on the Spinner
		spin.setAdapter(aa);
		spin1.setAdapter(aa1);
		spin2.setAdapter(aa2);
		spin3.setAdapter(aa3);

		return view;
	}

	// Initialize all views
	private void initViews() {
		fragmentManager = getActivity().getSupportFragmentManager();
		fullName = (EditText) view.findViewById(R.id.fullName);
		emailId = (EditText) view.findViewById(R.id.userEmailId);
		mobileNumber = (EditText) view.findViewById(R.id.mobileNumber);
		//location = (EditText) view.findViewById(R.id.location);
		password = (EditText) view.findViewById(R.id.password);
		confirmPassword = (EditText) view.findViewById(R.id.confirmPassword);
		signUpButton = (Button) view.findViewById(R.id.signUpBtn);
		login = (TextView) view.findViewById(R.id.already_user);
		terms_conditions = (CheckBox) view.findViewById(R.id.terms_conditions);
		txt_backtoview=(TextView)view.findViewById(R.id.txt_backtoview);

		/*// Setting text selector over textviews
		XmlResourceParser xrp = getResources().getXml(R.drawable.text_selector);
		try {
			ColorStateList csl = ColorStateList.createFromXml(getResources(),
					xrp);

			login.setTextColor(csl);
			terms_conditions.setTextColor(csl);
		} catch (Exception e) {
		}*/
	}

	// Set Listeners
	private void setListeners() {
		signUpButton.setOnClickListener(this);
		login.setOnClickListener(this);
		txt_backtoview.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.signUpBtn:

			// Call checkValidation method
			checkValidation();
			break;

		case R.id.already_user:

			// Replace login fragment
			new MainActivity().replaceLoginFragment();
			break;
			case R.id.txt_backtoview:
				backtologin();
		}

	}
	private void backtologin()
	{
		// Replace Login Fragment on Back Presses
		new MainActivity().replaceLoginFragment();
	}
	// Check Validation Method
	private void checkValidation() {

		// Get all edittext texts
		String getFullName = fullName.getText().toString();
		String getEmailId = emailId.getText().toString();
		String getMobileNumber = mobileNumber.getText().toString();
		//String getLocation = location.getText().toString();
		String getPassword = password.getText().toString();
		String getConfirmPassword = confirmPassword.getText().toString();

		// Pattern match for email id
		Pattern p = Pattern.compile(Utils.regEx);
		Matcher m = p.matcher(getEmailId);

		// Check if all strings are null or not
		if (getMobileNumber.equals("") || getMobileNumber.length() == 0
				//|| getLocation.equals("") || getLocation.length() == 0
				|| getPassword.equals("") || getPassword.length() == 0
				|| getConfirmPassword.equals("")
				|| getConfirmPassword.length() == 0)

			new CustomToast().Show_Toast(getActivity(), view,
					"All fields are required.");

	/*	// Check if email id valid or not
		else if (!m.find())
			new CustomToast().Show_Toast(getActivity(), view,
					"Your Email Id is Invalid.");

		// Check if both password should be equal
		else if (!getConfirmPassword.equals(getPassword))
			new CustomToast().Show_Toast(getActivity(), view,
					"Both password doesn't match.");
*/
		// Make sure user should check Terms and Conditions checkbox
		else if (!terms_conditions.isChecked())
			new CustomToast().Show_Toast(getActivity(), view,
					"Please select Terms and Conditions.");

		// Else do signup or do your stuff
		else
			// Replace forgot password fragment with animation
			fragmentManager
					.beginTransaction()
					.setCustomAnimations(R.anim.right_enter, R.anim.left_out)
					.replace(R.id.frameContainer,
							new OTPList_FragmentActivity(),
							Utils.OTPList_FragmentActivity).commit();
		Signup("testing4","9262482300","123456","11","12","3","BA");
			/*Toast.makeText(getActivity(), "Do SignUp.", Toast.LENGTH_SHORT)
					.show();*/

	}
	@Override
	public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

	}

	@Override
	public void onNothingSelected(AdapterView<?> adapterView) {

	}

	private  void Signup(String name,String mobileno,String pwd,String stateid,String districid,String regionid,String type)
	{
		Log.d("output_res1",name);
		Log.d("output_res2",mobileno);
		Log.d("output_res3",pwd);
		Log.d("output_res4",stateid);
		Log.d("output_res5",districid);
		Log.d("output_res6",regionid);
		Log.d("output_res7",type);



		ApiService service = RetroClient.getApiService();
		final Call<ResponseBody> add=service.SignUpPage(name,mobileno,pwd,stateid,districid,regionid,type);
		add.enqueue(new Callback<ResponseBody>() {
			@Override
			public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response)
			{
				Log.d("code",String.valueOf(response.code()));
				BufferedReader reader = null;
				StringBuilder sb = new StringBuilder();
				//  Log.d("code",String.valueOf(response.code()));
				if (response.code() == 200)
				{
					Log.d("hello","yes");
				} else
				{
					// Handle other response codes
					Log.d("hello","No");
				}
				Log.d("resultcode",String.valueOf(response.code()));
				String line=null;
				StringBuilder builder=new StringBuilder();
				reader = null;
				try
				{
					reader=new BufferedReader(new InputStreamReader(response.body().byteStream()));
					while ((line=reader.readLine())!=null)
					{
						builder.append(line);
					}
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}

				Log.d("result",builder.toString());



			}

			@Override
			public void onFailure(Call<ResponseBody> call, Throwable t)
			{
				Log.e("Error",t.toString());
			}
		});
	}
}
