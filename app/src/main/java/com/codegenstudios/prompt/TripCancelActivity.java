package com.codegenstudios.prompt;

import android.app.Activity;
import android.os.Bundle;
import android.view.Window;

import com.codegenstudios.prompt.R;

/**
 * Created by sathish on 11/19/2017.
 */
public class TripCancelActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_trip_cancel);
    }
}