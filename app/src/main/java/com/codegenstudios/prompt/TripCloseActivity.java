package com.codegenstudios.prompt;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

/**
 * Created by sathish on 11/19/2017.
 */
public class TripCloseActivity extends AppCompatActivity {
    Button tripclose;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_tripclose);
        tripclose=(Button)findViewById(R.id.tripclose);
        tripclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(TripCloseActivity.this,BookingReportActivity.class);
                startActivity(i);
            }
        });
    }
}