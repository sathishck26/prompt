package com.codegenstudios.prompt;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

/**
 * Created by sathish on 11/19/2017.
 */
public class WayBillActivity extends AppCompatActivity {
    TextView sharetoconsigner;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_way_bill);
        sharetoconsigner=(TextView)findViewById(R.id.sharetoconsigner);
        sharetoconsigner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(WayBillActivity.this,BookingReportActivity1.class);
                startActivity(i);
            }
        });
    }
}
