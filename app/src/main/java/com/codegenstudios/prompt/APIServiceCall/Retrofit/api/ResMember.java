package com.codegenstudios.prompt.APIServiceCall.Retrofit.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by SIT on 31-08-2017.
 */

public class ResMember
{
    @Expose
    @SerializedName("status")
    private String status;
    @Expose
    @SerializedName("msg")
    private String message;


    public String getStatus()
    {
        return status;
    }

    public String getMessage()
    {
        return message;
    }
}
