package com.codegenstudios.prompt.APIServiceCall.Retrofit.api;


import java.util.HashMap;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Streaming;
import retrofit2.http.Url;

/**
 * @author Pratik Butani on 23/4/16.
 */
public interface ApiService
{
    /*
    Retrofit get annotation with our URL
    And our method that will return us the List of Contacts
    */

    //,String userId,String imgId
    @Multipart
    @POST("api/api.php?rquest=image_upload")
    Call<Result> uploadImage(@Part MultipartBody.Part file);

    @Multipart
    @POST("api/api.php?rquest=user_call_to_action_button")
    Call<Result> uploadCreated(@Part MultipartBody.Part file, @Part("user_id") RequestBody user_id);


    @Multipart
    @POST("/api/api.php?rquest=user_call_to_action_button")
    Call<ResponseBody> uploadFileWithPartMap(@PartMap() HashMap<String, RequestBody> partMap, @Part MultipartBody.Part file);

    @Multipart
    @POST("/api/api.php?rquest=edit_user_call_to_action_button")
    Call<ResponseBody> Regenerate_lib_edit_WithPartMap(@PartMap() HashMap<String, RequestBody> partMap, @Part MultipartBody.Part file);

    @POST("api/api.php?rquest=AndroidProductPaymentInfo")
    @FormUrlEncoded
    Call<ResMember> sendCartData(@FieldMap HashMap<String, String> params);

    @POST("api/api.php?rquest=AndroidTopUp")
    @FormUrlEncoded
    Call<TopUpResult> Topup(@FieldMap HashMap<String, String> params);


    @Streaming
    @GET
    Call<ResponseBody> getApng(@Url String url);

    @Multipart
    @POST("api/api.php?rquest=image_upload")
    Call<ResponseBody> uploadImage(@PartMap() HashMap<String, RequestBody> partMap, @Part MultipartBody.Part image_file, @Part MultipartBody.Part thumbFile);

    @POST("api/api.php?rquest=video_overlay_image")
    @FormUrlEncoded
    Call<PngList> getApngs(@Field("User_ID") String userId);


    @POST("api/api.php?rquest=save_user_selected_overlay_images")
    @FormUrlEncoded
    Call<ResponseBody> addPng(@Field("User_ID") String userId, @Field("OverlayImageId") String overlayImageId);

    @POST("api/api.php?rquest=view_all_video_overlay_images")
    @FormUrlEncoded
    Call<AllPng> getAllPngs(@Field("User_ID") String userId, @Field("category") String category);



    @GET("api/api.php?rquest=view_ffmpeg_video_effects")
    Call<AllVideoEffects> getAllOverlayEffect();

    @Multipart
    @POST("api/api.php?rquest=image_upload_final")
    Call<ResponseBody> uploadMultipleFiles(@PartMap() HashMap<String, RequestBody> partMap, @Part List<MultipartBody.Part> image_file, @Part List<MultipartBody.Part> thumbFile);


    @Multipart
    @POST("/api/api.php?rquest=call_to_action_module")
    Call<ResponseBody> use_video_uploadFileWithPartMap(@PartMap() HashMap<String, RequestBody> partMap, @Part MultipartBody.Part file);


    @POST("api/businessassociate/register")
    @FormUrlEncoded
    Call<ResponseBody> SignUpPage(@Field("name") String name, @Field("mobileNo") String mobileno,@Field("password") String password,@Field("stateId") String stateid,@Field("districtId") String districid,@Field("regionId") String regionid,@Field("type") String type);

}
