package com.codegenstudios.prompt.APIServiceCall.Retrofit.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by SIT on 9/20/2017.
 */

public class AllVideoEffects implements Serializable
{

    @SerializedName("view_ffmpeg_video_effects")
    @Expose
    private List<AllEffectVideo> pngs=null;

    public ArrayList<AllEffectVideo> getPngs()
    {
        return (ArrayList<AllEffectVideo>) pngs;
    }

    public void setPngs(ArrayList<AllEffectVideo> pngs)
    {
        this.pngs = pngs;
    }

}
