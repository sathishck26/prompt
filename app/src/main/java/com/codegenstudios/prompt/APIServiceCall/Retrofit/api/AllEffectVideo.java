package com.codegenstudios.prompt.APIServiceCall.Retrofit.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by SIT on 9/20/2017.
 */

public class AllEffectVideo
{
    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("path")
    @Expose
    private String image_path;

    @SerializedName("name")
    @Expose
    private String overlay_txt;

    public String getId() {
        return id;
    }

    public String getImage_path() {
        return image_path;
    }

    public String getOverlay_txt() {
        return overlay_txt;
    }


}
