package com.codegenstudios.prompt.APIServiceCall.Retrofit.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by SIT on 8/29/2017.
 */

public class AllPng implements Serializable
{
    @SerializedName("view_all_video_overlay_images")
    @Expose
    private List<AllPngIndividual> pngs=null;

    public ArrayList<AllPngIndividual> getPngs()
    {
        return (ArrayList<AllPngIndividual>) pngs;
    }

    public void setPngs(ArrayList<AllPngIndividual> pngs) {
        this.pngs = pngs;
    }
}
