package com.codegenstudios.prompt.APIServiceCall.Retrofit.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by SIT on 8/29/2017.
 */

public class AllPngIndividual
{

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("image_path")
    @Expose
    private String image_path;

    @SerializedName("image_text")
    @Expose
    private String overlay_txt;

    @SerializedName("thumb_image")
    @Expose
    private String thumb_image_path;

    @SerializedName("image_status")
    @Expose
    private String image_status;

    public String getId() {
        return id;
    }

    public String getImage_path() {
        return image_path;
    }

    public String getOverlay_txt() {
        return overlay_txt;
    }

    public String getThumb_image_path() {
        return thumb_image_path;
    }

    public String getImage_status() {
        return image_status;
    }
}
