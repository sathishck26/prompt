package com.codegenstudios.prompt.APIServiceCall.Retrofit.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by SIT on 8/23/2017.
 */

public class Apng
{
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("image_path")
    @Expose
    private String image_path;
    @SerializedName("image_text")
    @Expose
    private String overlay_txt;
    @SerializedName("thumb_image")
    @Expose
    private String thumb_image_path;
    @SerializedName("image_status")
    @Expose
    private String image_status;



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage_path()
    {
        return image_path;
    }

    public void setImage_path(String image_path) {
        this.image_path = image_path;
    }

    public String getThumb_image_path() {
        return thumb_image_path;
    }

    public void setThumb_image_path(String thumb_image_path) {
        this.thumb_image_path = thumb_image_path;
    }

    public String getImage_status() {
        return image_status;
    }

    public void setImage_status(String image_status) {
        this.image_status = image_status;
    }

    public String getOverlay_txt()
    {
        return overlay_txt;
    }

    public void setOverlay_txt(String overlay_txt)
    {
        this.overlay_txt = overlay_txt;
    }
}
