package com.codegenstudios.prompt.APIServiceCall.Retrofit.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by SIT on 8/23/2017.
 */

public class PngList implements Serializable
{
    @SerializedName("view_video_overlay_image")
    @Expose
   private List<Apng> pngs=null;

    public ArrayList<Apng> getPngs()
    {
        return (ArrayList<Apng>) pngs;
    }

    public void setPngs(ArrayList<Apng> pngs) {
        this.pngs = pngs;
    }
}
