package com.codegenstudios.prompt.APIServiceCall.Retrofit.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by SIT on 30-Aug-17.
 */

public class TopUpResult
{
    @Expose
    @SerializedName("Status")
    private String status;
    @Expose
    @SerializedName("Message")
    private String message;


    public String getStatus()
    {
        return status;
    }

    public String getMessage()
    {
        return message;
    }
}
