package com.codegenstudios.prompt;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;

/**
 * Created by sathish on 11/19/2017.
 */
public class PopupActivity extends Activity {
    LinearLayout ll_firstpopup,ll_secondpopup;
    Button btn_ok1,btn_ok;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.popuplayout);
        ll_firstpopup=(LinearLayout)findViewById(R.id.ll_firstpopup);
        ll_secondpopup=(LinearLayout)findViewById(R.id.ll_secondpopup);
        btn_ok1=(Button)findViewById(R.id.btn_ok1);
        btn_ok=(Button)findViewById(R.id.btn_ok);
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ll_firstpopup.setVisibility(View.GONE);
                ll_secondpopup.setVisibility(View.VISIBLE);
            }
        });
        btn_ok1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(PopupActivity.this,TripActivity.class);
                startActivity(i);
            }
        });

    }
}