package com.codegenstudios.prompt;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

/**
 * Created by sathish on 11/19/2017.
 */
public class TripStartActivity extends AppCompatActivity {
    Button tripStart;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_tripstart);
        tripStart=(Button)findViewById(R.id.tripStart);
        tripStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(TripStartActivity.this,WayBillActivity.class);
                startActivity(i);
            }
        });
    }
}